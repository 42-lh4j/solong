/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_make_sprite.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: meow <meow@student.1337.ma>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/04 19:00:09 by meow              #+#    #+#             */
/*   Updated: 2024/05/07 02:04:15 by meow             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <so_long.h>

static char	*get_image_path(t_obj_type type)
{
	if (type == INVALID)
		return (NULL);
	if (type == COLLECTIBLE)
		return ("collectible/food.xpm");
	if (type == WALL)
		return ("obstacle/wall.xpm");
	if (type == DOOR_LOCKED)
		return ("exit/locked.xpm");
	if (type == DOOR_UNLOCKED)
		return ("exit/unlocked.xpm");
	if (type == PLAYER)
		return ("player/luffy-left.xpm");
	return (NULL);
}

/**
 * @brief init's a sprite according to the passed type !
 * @param type the object type !
 * @param mlxptr a pinter that is required by mlx function !
 * @return a pointer to a sprite struct !
 * @fixme handle when the image fp <file path> isn't exists !
 */
t_sprite	*ft_mksprite(t_obj_type type, void *mlxptr, char *impath_)
{
	t_sprite	*sprite;
	char		*impath;

	if (impath_)
		impath = impath_;
	sprite = (t_sprite *)malloc(sizeof(t_sprite));
	ft_memset(
			(void *)sprite,
			0,
			sizeof(t_sprite)
			); /** @fixme with a memset ... done ! */
	if (sprite == NULL)
		return (NULL);
	else if (type != NONE)
	{
		impath = get_image_path(type);
		if (impath == NULL)
		{
			free(sprite);
			return (NULL);
		}
	}
	impath = ft_strjoin(XPM_BASE_PATH, impath);
	sprite->imptr = mlx_xpm_file_to_image(
			mlxptr, impath,
			&sprite->size.y, &sprite->size.x
			);
	if (sprite->imptr == NULL)
	{
		free(impath);
		free(sprite);
		return (NULL);
	}
	free(impath);
	return (sprite);
}
