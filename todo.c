/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   todo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: meow <meow@student.1337.ma>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/07 02:33:11 by meow              #+#    #+#             */
/*   Updated: 2024/05/07 02:44:11 by meow             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**

@todo	[-] import the project
@note		+ norminated
@fixme		+ fixed (no sigfaults, error handled ...)
@fixme		+ without leaks
@note		+ with a makefile instead of cmakefile

@fixme	[-] that valgrind syscall error !!!
@fixme	[-] handle (with memcheck !) (fixed)
@note		+ invalid files
 			+ missing / renamed files
 			+ corrupted files ... etc !

@todo	[-] fix norminette
@note		+ split all those functions
	
@todo	[-] fix the errors from valgrind (done)
@note		+ use memset to clear every heap allocated struct (checked)
	
@todo	[-] fix the case where the min required coin is 1 (done)
@note		+ return invalid map when 0 coin (checked)
@note		+ print an error when an invalid map provided ! (checked)

@todo	[-] fix the memory leaks (done)
@fixme		+ the extra case where 'c' is in the map (fixed)

*/
